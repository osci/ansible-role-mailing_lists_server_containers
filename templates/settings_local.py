import os
from socket import gethostbyname

ADMINS = (
     ('Mailman Suite Admin', '{{ site_owner_email }}'),
)
DEFAULT_FROM_EMAIL = 'mailman@{{ mail_domain }}'
SERVER_EMAIL = 'listmaster@{{ mail_domain }}'

ALLOWED_HOSTS = [
    "localhost",  # Archiving API from Mailman, keep it.
    "mailman-web",
    gethostbyname("mailman-web"),
    os.environ.get('SERVE_FROM_DOMAIN'),
    os.environ.get('DJANGO_ALLOWED_HOSTS'),
{% for vhost in extra_vhosts %}
    vhost,
{% endfor %}
]

from settings import TEMPLATES
TEMPLATES[0]['DIRS'].append('/opt/mailman-web-data/templates')

# Additional locations of static files
# (the 'static' directory in the mounted data directory gets regenerated)
STATICFILES_DIRS = (
    "/opt/mailman-web-data/static-extra/",
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': "/opt/mailman-web-data/fulltext_index",
    },
}

# Django Allauth
SOCIALACCOUNT_PROVIDERS = {
    'openid': {
        'SERVERS': [
{% for service_name, service_data in mailman_auth.items() %}
{% if service_data.provider == 'openid' %}
            dict(id='{{ service_name }}',
                 name='{{ service_data.display_name }}',
                 openid_url='{{ service_data.url }}'),
{% endif %}
{% endfor %}
        ],
    },
    'google': {
        'SCOPE': ['profile', 'email'],
        'AUTH_PARAMS': {'access_type': 'online'},
    },
}
MAILMAN_WEB_SOCIAL_AUTH = [
{% if with_fedora_auth %}
    # Fedora Auth is based on OpenID and related DB tables need to be initialized
    'allauth.socialaccount.providers.openid',
    'django_mailman3.lib.auth.fedora',
{% endif %}
{% for service_name, service_data in mailman_auth.items() %}
    'allauth.socialaccount.providers.{{ service_data.provider }}',
{% endfor %}
]

#
# Gravatar
# https://github.com/twaddington/django-gravatar
#
# Gravatar base url.
GRAVATAR_URL = 'http://cdn.libravatar.org/'
# Gravatar base secure https url.
GRAVATAR_SECURE_URL = 'https://seccdn.libravatar.org/'
# Gravatar size in pixels.
#GRAVATAR_DEFAULT_SIZE = '80'
# An image url or one of the following: 'mm', 'identicon', 'monsterid', 'wavatar', 'retro'.
GRAVATAR_DEFAULT_IMAGE = 'retro'
# One of the following: 'g', 'pg', 'r', 'x'.
#GRAVATAR_DEFAULT_RATING = 'g'
# True to use https by default, False for plain http.
GRAVATAR_DEFAULT_SECURE = True

#DEBUG=True

