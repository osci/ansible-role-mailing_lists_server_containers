# Ansible role to install a complete Mailing-Lists server

## Introduction

This role installs and configure all needed softwares to get a complete
Mailing-Lists server with administrative webui and anti-spam.

While the previous `mailing_lists_server` role was using packages to deploy all pieces we now use containers to deploy Mailman 3 components as the packages are lacking (at the time of writing only the server core was packaged are mostly maintained). This also should simplify using new versions as they often require bleeding edge versions of some Python libraries without having to wait for the next major version of the OS.

The following softwares are used ([C] are containers):

- [C] Mailman 3 core
- [C] Mailman 3 web (hyperkitty, postorius)
- [C] PostgreSQL
- Apache HTTPd
- Postfix
- Spamassassin
- Postgrey

Configuration example:

```
- name: "Deploy Mailing-Lists"
  hosts: lists.example.com
  tasks:
      - name: "Install Mailing-Lists Instance"
        include_role:
            name: mailing_lists_server_containers
        vars:
            mail_domain: lists.example.com
            mail_aliases:
              root: ['root', 'admin@example.com']
              listmaster: root
            whitelist_clients:
              - example.com
            site_admins:
              - theadmin
```

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role mailing_lists_server_containers

```

## Customizations

### Mail templates

```
    - name: "Install Mail Templates"
      ansible.builtin.copy:
        src: "{{ data_dir }}/mail_templates/{{ item }}"
        dest: "/home/mailman/core/var/templates/{{ item }}"
        owner: mailman
        group: mailman
        mode: 0644
      loop:
        - "site/en/list:member:regular:footer.txt"
        - "site/en/list:member:digest:footer.txt"
```

### Navbar Logo

You need to customize the navbar to add your logo, then add the logo to the assets:

```
    - name: "Install custom branded navbar template"
      ansible.builtin.copy:
        src: "{{ data_dir }}/navbar-brand.html"
        dest: "/home/mailman/web/templates/hyperkitty/"
        owner: mailman
        group: mailman
        mode: 0644
      notify: Restart Mailman Web container

    - name: "Install custom logo"
      ansible.builtin.copy:
        src: "{{ data_dir }}/{{ item }}"
        dest: "/home/mailman/web/static-extra/"
        owner: mailman
        group: mailman
        mode: 0644
      loop:
        - tenant_logo.png
        # other assets
      notify: Restart Mailman Web container
```

Example of navbar template (`navbar-brand.html`):

```
{% load static %}
<a class="navbar-brand" href="{% url 'hk_root' %}" title="{{ site_name }}">
    <img alt="{{ site_name|title }}" src="{% static 'tenant_logo.png' %}" style="float: left; margin-right: 30px; height: 40px; padding: 0; margin-top: -10px;" />
    {{ site_name }}
</a>
```

### Authentication

The `mailman_auth` variable holds the list of authentication system you with to add to the default local database. The `display_name` is free, the `provider` corresponds to the `allauth` provider name, and the `client_id` and `client_secret` needs to be [generated on the provider website](https://osci.io/offers/mailing-lists_tokens/).

Pass the `mailman_auth` variable to the role parameters (use Ansible vault to hide secrets, it is in clear here for the sake of understanding):

```
- name: "Deploy Mailing-Lists"
  hosts: lists.example.com
  tasks:
      - name: "Install Mailing-Lists Instance"
        include_role:
            name: mailing_lists_server_containers
        vars:
            …
            mailman_auth:
              gitlab:
                display_name: GitLab
                provider: gitlab
                client_id: "exampleexampleexampleexampleexampleexampleexampleexampleexample"
                client_secret: "exampleexampleexampleexampleexampleexampleexampleexampleexample"
```

Fedora authentication is handled specially and can be enabled using the `with_fedora_auth` variable.
