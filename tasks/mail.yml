---

- name: "Install SpamAssassin"
  include_role:
    name: spamassassin
  vars:
    service_profile: medium

- name: "Install Postgrey"
  include_role:
    name: postgrey

- name: "Install MTA"
  include_role:
    name: postfix
  vars:
    myhostname: "{{ inventory_hostname }}"
    mydomain: "{{ domain }}"
    with_postgrey: true
    with_mailman3: true
    with_spamassassin: true
    smtpd_options:
      content_filter: spamfilter
    aliases: "{{ mail_aliases }}"
    tls_method: manual
    cert_file: "/etc/letsencrypt/live/{{ webui_vhost }}/fullchain.pem"
    key_file: "/etc/letsencrypt/live/{{ webui_vhost }}/privkey.pem"
    ca_file: /etc/pki/tls/cert.pem
    # omit does not work here, cannot use a clearer variable name
    #auth: "{{ postfix_auth | default(omit) }}"
    # postmap is not available inside the mailman-core container therefore the hash scheme cannot be used
    mailman3_core_dir: /home/mailman/core/var/
    mailman3_maps_scheme: regexp

- name: "Give postfix access to mailman maps"
  ansible.builtin.user:
    name: postfix
    groups: mailman
    append: yes
  notify: restart mail system

- name: "Add selinux module to allow postfix to read mailman-generated config"
  block:
    - name: "Create directory for SELinux module preparation"
      file:
        path: "{{ _ml_se_path }}"
        state: directory
        owner: root
        group: root
        mode: 0755

    - name: "Install SELinux module sources"
      copy:
        src: "{{ _ml_se_mod_base }}.te"
        dest: "{{ _ml_se_path }}/"
        owner: root
        group: root
        mode: 0644
      notify:
        - Regenerate postfix SELinux module
        - restart mail system

# mailman-core provides the LMTP service and advertise it in the generated maps
# using its internal IP (in the mailman dedicated network) but the host cannot
# access it. The port is published on localhost though, so let's add a redirect.
# Widening the redirect in order to catch all these cases in one go.
#
# The ansible firewalld module does not support direct rules.
# A PR in the old repository was neve rcontinued in the collection:
#   https://github.com/ansible/ansible/pull/63772
# instead of trying to find the state, override Firewalld config directly
- name: "Redirect Mailman network traffic to localhost"
  when: manage_firewall|bool
  template:
    src: direct.xml
    dest: /etc/firewalld
    owner: root
    group: root
    mode: 0644
  notify: Restart Firewalld

- name: "Install MDA"
  include_role:
    name: dovecot
  vars:
    auth: "{{ dovecot_auth | default('yaml-dict') }}"
    cert_file: "/etc/letsencrypt/live/{{ webui_vhost }}/fullchain.pem"
    key_file: "/etc/letsencrypt/live/{{ webui_vhost }}/privkey.pem"
    ca_file: /etc/pki/tls/cert.pem
  when: with_dovecot|bool

# LE hooks order:
#   - 03: deploy cert if needed (copy and change perms for non-root services)
#   - 07: restart/reload/systemctl kill/… to notify the service to take the new cert into account
# other levels for special actions
- name: "Install Let's Encrypt renewal hook"
  template:
    src: "07_mailing_lists_server_cert_renewal_restart_service"
    dest: /etc/letsencrypt/renewal-hooks/deploy/
    owner: root
    group: root
    mode: 0755

